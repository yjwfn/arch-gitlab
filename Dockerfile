FROM openjdk:8-alpine
MAINTAINER yjwfn <head_main@qq.com>


ARG JAR_FILE
RUN mkdir /app
ADD target/${JAR_FILE} /app/app.jar
WORKDIR /app
ENTRYPOINT ["/usr/bin/java", "-jar", "/app/app.jar"]
