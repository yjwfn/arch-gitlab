package com.javaobj.gitlab;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/")
public class HelloController {

    @Autowired
    private Environment environment;


    @GetMapping
    public ResponseEntity<String> hello(){

        return ResponseEntity.ok(
                "Hello, World, run in " + Arrays.toString(environment.getActiveProfiles())
        );
    }
}
